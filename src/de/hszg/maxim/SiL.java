package de.hszg.maxim;

import de.hszg.maxim.service.CarMotorOutput;
import de.hszg.maxim.service.CarSensorInput;

import static java.lang.Thread.sleep;

public class SiL implements CarMotorOutput, CarSensorInput, Runnable {

    private int speed;
    private double vl, vr, hl, hr;

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public void steering(int x) {

    }

    @Override
    public double getDistance(Sensor s) {
        if (s.equals(Sensor.VL)) return vl;
        if (s.equals(Sensor.HL)) return hl;
        if (s.equals(Sensor.VR)) return vr;
        if (s.equals(Sensor.HR)) return hr;
        return 0.0;
    }

    @Override
    public void run() {
        while (true) {
            try {
                test();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        SiL t = new SiL();
        Controller controller = new Controller(t, t);
        controller.update();
        t.run();
    }

    private void test() throws InterruptedException {
        vl = 100;
        vr = 100;
        sleep(500);
        assert speed > 0 : "test_1 ERROR: Speed is incorrect";
        System.out.println("test_1 passed");

        hl = 10;
        hr = 10;
        sleep(500);
        assert speed > 0 : "test_2 ERROR: Speed is incorrect";
        System.out.println("test_2 passed");

        hl = 12;
        hr = 12;
        sleep(500);
        assert speed != 0 : "test_3 ERROR: Speed is incorrect";
        System.out.println("test_3 passed");

        vl = 9;
        vr = 9;
        sleep(500);
        assert speed == -100 : "test_4 ERROR: Speed is incorrect";
        System.out.println("test_4 passed");
    }
}
