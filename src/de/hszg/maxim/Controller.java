package de.hszg.maxim;

import de.hszg.maxim.exception.CarException;
import de.hszg.maxim.service.CarMotorOutput;
import de.hszg.maxim.service.CarSensorInput;

import static java.lang.Thread.sleep;

class Controller {
    private CarSensorInput in;
    private CarMotorOutput out;

    Controller(CarSensorInput i, CarMotorOutput o) {
        in = i;
        out = o;
    }

    void update() {
        new Thread(() -> {
            try {
                while (true) {
                    if (in.getDistance(CarSensorInput.Sensor.VL) < 50.0 || in.getDistance(CarSensorInput.Sensor.VR) < 50.0) {
                        out.setSpeed(-100);
                    } else if (in.getDistance(CarSensorInput.Sensor.HL) < 50.0 || in.getDistance(CarSensorInput.Sensor.HR) < 50.0) {
                        out.setSpeed(100);
                    } else {
                        out.setSpeed(200);
                    }
                    sleep(1);
                }
            } catch (CarException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

}