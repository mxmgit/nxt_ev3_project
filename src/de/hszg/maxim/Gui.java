package de.hszg.maxim;

import de.hszg.maxim.service.CarMotorOutput;
import de.hszg.maxim.service.CarSensorInput;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Gui extends Application implements CarMotorOutput, CarSensorInput {
    private static int speed;
    private static double vl, vr, hl, hr;

    @Override
    public void setSpeed(int speed) {
        Gui.speed = speed;
    }

    @Override
    public void steering(int x) {
    }

    @Override
    public double getDistance(Sensor s) {
        if (s.equals(Sensor.VL)) return vl;
        if (s.equals(Sensor.HL)) return hl;
        if (s.equals(Sensor.VR)) return vr;
        if (s.equals(Sensor.HR)) return hr;
        return 0.0;
    }

    private GridPane gridPane = new GridPane();

    private Slider sliderVl;
    private Slider sliderVr;
    private Slider sliderHl;
    private Slider sliderHr;
    private TextArea textArea;


    @Override
    public void start(Stage primaryStage) {

        sliderVl = new Slider();
        sliderVr = new Slider();
        sliderHl = new Slider();
        sliderHr = new Slider();
        textArea = new TextArea();

        primaryStage.setTitle("Nxt GUI");
        Pane root = new Pane();
        VBox vBox = new VBox();

        root.getChildren().add(vBox);
        vBox.getChildren().add(gridPane);
        vBox.getChildren().add(textArea);

        setSliders();
        setGridPane();

        sliderVl.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    Gui.vl = (double) newValue;
                    textArea.appendText("\n Logging: \n speed =  " + Gui.speed +
                            "\n Sensor.VL =  " + Gui.vl +
                            "\n Sensor.VR =  " + Gui.vr +
                            "\n Sensor.HL =  " + Gui.hl +
                            "\n Sensor.HR =  " + Gui.hr + "\n"
                    );
                });

        sliderVr.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    vr = (double) newValue;
                    textArea.appendText("\n Logging: \n speed =  " + speed +
                            "\n Sensor.VL =  " + vl +
                            "\n Sensor.VR =  " + vr +
                            "\n Sensor.HL =  " + hl +
                            "\n Sensor.HR =  " + hr + "\n"
                    );
                });

        sliderHl.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    hl = (double) newValue;
                    textArea.appendText("\n Logging: \n speed =  " + speed +
                            "\n Sensor.VL =  " + vl +
                            "\n Sensor.VR =  " + vr +
                            "\n Sensor.HL =  " + hl +
                            "\n Sensor.HR =  " + hr + "\n"
                    );
                });

        sliderHr.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    hr = (double) newValue;
                    textArea.appendText("\n Logging: \n speed =  " + speed +
                            "\n Sensor.VL =  " + vl +
                            "\n Sensor.VR =  " + vr +
                            "\n Sensor.HL =  " + hl +
                            "\n Sensor.HR =  " + hr + "\n"
                    );
                });

        vl = sliderVl.getValue();

        Scene scene = new Scene(root, 480, 330);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Gui gui = new Gui();
        Controller controller = new Controller(gui, gui);
        controller.update();
        Application.launch(Gui.class, args);
    }


    private void setSliders() {
        sliderVl.setShowTickMarks(true);
        sliderVl.setShowTickLabels(true);
        sliderVr.setShowTickMarks(true);
        sliderVr.setShowTickLabels(true);
        sliderHl.setShowTickMarks(true);
        sliderHl.setShowTickLabels(true);
        sliderHr.setShowTickMarks(true);
        sliderHr.setShowTickLabels(true);
    }

    private void setGridPane() {
        gridPane.add(sliderVl, 1, 1, 1, 1);
        gridPane.add(sliderHl, 1, 3, 1, 1);
        gridPane.add(sliderVr, 3, 1, 1, 1);
        gridPane.add(sliderHr, 3, 3, 1, 1);
        gridPane.setHgap(30);
        gridPane.setVgap(30);
    }
}
